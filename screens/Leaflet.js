import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Alert } from "react-native";
import WebViewLeaflet from "react-native-webview-leaflet";
import {Button} from "../components";

export default class Map extends Component {
    constructor() {
        super();
        this.state = {
            location: null,
            errorMessage: null,
            locations: [],
            coords: undefined,
            showEmojiSelectorModal: false,
            mapState: {
                showAttributionControl: false,
                showZoomControls: false,
                panToLocation: false,
                zoom: 10
            }
        };
    }

    componentDidMount() {
        this.setState({
            coords: [6.34511, 45.2768] //<<===Expected to point to a specific coord
        });
    }

    componentDidUpdate = (prevProps, prevState) => {
        // do these things once the map has loaded
        if (!prevState.mapState.mapLoaded && this.state.mapState.mapLoaded) {
            this.webViewLeaflet.sendMessage({
                centerPosition: { lat: 6.34511, lng: 45.2768 }
            });
        }

        // do these things only if the map has been loaded
        if (this.state.mapState.mapLoaded) {
            // if the user's location has changed
            if (prevState.coords !== this.state.coords) {
                this.webViewLeaflet.sendMessage({
                    centerPosition: this.state.coords,

                    locations: [
                        ...this.state.locations,
                        {
                            id: 0,
                            coords: this.state.coords,
                            icon: "❤️",
                            size: [24, 24],
                            animation: {
                                name: "pulse",
                                duration: ".5",
                                delay: 0,
                                interationCount: "infinite"
                            }
                        }
                    ]
                });
            }
        }
    };

    onLoad = event => {
        this.setState({
            ...this.state,
            mapState: { ...this.state.mapState, mapLoaded: true }
        });
        console.log("onLoad received : ", event);
    };

    onUpdateMapState = data => {
        this.setState({
            ...this.state,
            mapState: { ...this.mapState, ...data }
        });
    };

    render() {
        return (
            <View style={styles.container}>
                <Text
                    style={{
                        margin: 10,
                        fontSize: 24,
                        color: "white"
                    }}
                >
                    Animated Map Markers App
                </Text>

                <WebViewLeaflet
                    // get a reference to the web view so that messages can be sent to the map
                    ref={component => (this.webViewLeaflet = component)}
                    // the component that will receive map events
                    eventReceiver={this}
                />
                <View
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-around",
                        alignItems: "center",
                        paddingVertical: 8,
                        backgroundColor: "rgba(255,255,255,.50)"
                    }}
                >
                    <Button
                        onPress={() => this.centerMap("dw")}
                        borderWidth={0}
                        fontSize={30}
                        text={"🏰"}
                    />
                    <Button
                        onPress={() => this.centerMap("bg")}
                        borderWidth={0}
                        fontSize={30}
                        text={"🍺"}
                    />
                    <Button
                        onPress={() => this.centerMap("kd")}
                        borderWidth={0}
                        fontSize={30}
                        text={"👑"}
                    />
                    <Button
                        // onPress={this.setBoundsForAllMarkers}
                        borderWidth={0}
                        fontSize={30}
                        text={"🗺️"}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: "#6C6363",
        display: "flex"
    },
    statusBar: {
        height: 0
    },
    controlButton: {
        height: 40,
        width: 40,
        borderRadius: 5,
        backgroundColor: "dodgerblue"
    }
});