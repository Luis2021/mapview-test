import React from "react";
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    Modal,
    TextInput

} from "react-native";
import {Button, Item, Input, MapViewExample} from "../components";
import MapView from 'react-native-maps';
import {Marker, Polygon} from 'react-native-maps';
import ListItem from "../components/ListItem";

class App extends React.Component {
    state = {
        data: [],
        isVisible: false,
        text: "",
        numero: "",
        rue: "",
        cp: "",
        ville: "",
        addrese: "",
        markers: [],
        polygon: [
            {
                latitude: 46.0000000,
                longitude: 2.0000000,
            }
        ],
        region: {
            latitude: 46.0000000,
            longitude: 2.0000000,
            latitudeDelta: 8.0922,
            longitudeDelta: 4.0421,
        }
    };

    handlePress = () => {
        this.setState({isVisible: true});
    };
    handleChangeNumero = numero => this.setState({numero});
    handleChangeRue = rue => this.setState({rue});
    handleChangeCP = cp => this.setState({cp});
    handleChangeVille = ville => this.setState({ville});
    goToMarker = (item) => {
        const region =
            {
                latitude:item.coordinates.latitude,
                longitude:item.coordinates.longitude,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421
            }
        this.setState({region})
    } //Methode pour mettre a jour la region
    fetchMarker = async (addrese) => {
        const response = await fetch('https://api-adresse.data.gouv.fr/search/?q=' + addrese)
        const adresses = await response.json();
        const markers = adresses.features.map((item) => {
            return {
                key: Math.random().toString(),
                title: addrese,
                coordinates: {
                    latitude: item.geometry.coordinates[1],
                    longitude: item.geometry.coordinates[0]
                }
            };
        });

        const joined = this.state.markers.concat(markers);
        this.setState({markers: joined})
    }; //Methode pour obtenir les donnes depuis l'API
    handleSave = () => {
        const addrese = this.state.numero + " " + this.state.rue + " " + this.state.cp + " " + this.state.ville;

        this.setState({
            isVisible: false,
            text: "",
            numero: "",
            rue: "",
            cp: "",
            ville: "",
            addrese: addrese
        });
        this.fetchMarker(addrese)
    }; //Mise a jour de state

    render() {
        const {markers} = this.state;
        return (
            <View style={[styles.container, styles.gray]}>
                <View style={styles.header}>
                    <Text style={styles.title}>Ajuter marqueur de ville</Text>
                </View>
                <View style={styles.view}>
                    <Button title="Ajouter" onPress={this.handlePress}/>

                    <Button title="Aller vers Leaflet" onPress={() =>
                        this.props.navigation.push('Leaflet')}/>
                </View>
                <FlatList
                    data={markers}
                    keyExtractor={x => String(x.key)}
                    renderItem={({item}) => <ListItem title={item.title} onPress={() => this.goToMarker(item)} /> }
                            />

                <Modal animationType="slide" visible={this.state.isVisible}>
                    <View style={[styles.container, styles.center]}>
                        <Text style={styles.modalTitle}>Ecrivez l'addrese</Text>
                        <Input
                            onChangeText={this.handleChangeNumero}
                            placeholder="Numero"
                            value={this.state.numero}
                            keyboardType='numeric'
                        />
                        <Input
                            onChangeText={this.handleChangeRue}
                            placeholder="Rue"
                            value={this.state.rue}
                        />
                        <Input
                            onChangeText={this.handleChangeCP}
                            placeholder="CP"
                            keyboardType='numeric'
                            value={this.state.cp}
                        />
                        <Input
                            onChangeText={this.handleChangeVille}
                            placeholder="Ville"
                            value={this.state.ville}
                        />
                        <Button title="Confirmer" onPress={this.handleSave}/>

                    </View>
                </Modal>

                <MapView
                    style={styles.map}
                    showsUserLocation={false}
                    followUserLocation={false}
                    zoomEnabled={true}
                    initialRegion={
                        {
                            latitude: 46.0000000,
                            longitude: 2.0000000,
                            latitudeDelta: 8.0922,
                            longitudeDelta: 4.0421,
                        }
                    }
                    region={this.state.region}

                    onPress={(event) => {
                        const coordinates = event.nativeEvent.coordinate;
                        const joined = this.state.polygon.concat(coordinates);
                        this.setState({polygon: joined})

                    }}
                >
                    
                    {this.state.markers.map((marker, index) => (
                        <MapView.Marker
                            key={index}
                            coordinate={marker.coordinates}
                            title={marker.title}
                        />

                    ))}

                    <Polygon coordinates={this.state.polygon}/>

                </MapView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff"
    },
    center: {
        justifyContent: "center",
        alignItems: "center"
    },
    header: {
        height: 75,
        borderBottomWidth: 1,
        borderBottomColor: "#ddd"
    },
    title: {
        textAlign: "center",
        marginTop: 25,
        fontSize: 28
    },
    view: {
        height: 50,
        margin: 15,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: 'space-between'
    },
    gray: {
        backgroundColor: "#eee"
    },
    modalTitle: {
        fontSize: 28
    },
    map: {
        height: 250,

    }
});

export default App;