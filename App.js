import { StyleSheet, Text, View } from 'react-native';
import {createAppContainer} from 'react-navigation';
import { createStackNavigator } from "react-navigation-stack";
import GoogleScreen from './screens/Google';
import LeafletScreen from './screens/Leaflet';


const AppNavigator = createStackNavigator({
    Google: {
        screen : GoogleScreen,
    },
    Leaflet: {
        screen : LeafletScreen,
    }
},{
    initialRouteName: 'Google',
})

export default createAppContainer(AppNavigator)
