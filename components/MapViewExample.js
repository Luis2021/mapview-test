import React, { Component } from 'react'
import {
   StyleSheet
} from 'react-native'
import MapView from 'react-native-maps';

export default MapViewExample = (props) => {

   return (
      
      <MapView
         style = {styles.map}
         showsUserLocation = {false}
         followUserLocation = {false}
         zoomEnabled = {true}
         initialRegion={{
            latitude: 19.4284706,
            longitude: -99.1276627,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
         }}
      >
          <MapView.Marker
              coordinate={{latitude: 37.78825,
                  longitude: -122.4324}}
              title={"title"}
              description={"description"}
          />
      </MapView>
   )
}

const styles = StyleSheet.create ({
   map: {
      height: 400,
      marginTop: 80
   }
})