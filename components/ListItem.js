import React from 'react'
import {TouchableOpacity, Text} from 'react-native'

export default ({title, onPress}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <Text style={{ fontSize: 20}}> {title} </Text>
        </TouchableOpacity>
    )
}